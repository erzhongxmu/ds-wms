package com.zhaodui.tencent.sdk;

import com.zhaodui.fxy.JSONHelper;
import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class WXUtil {
    /**
     * 生成随机字符串
     * @return
     */
    public static String getNonceStr() {
        Random random = new Random();
        return MD5Util.MD5Encode(String.valueOf(random.nextInt(10000)), "utf8");
    }
    /**
     * 获取时间戳
     * @return
     */
    public static String getTimeStamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    public static String getJsApiTicket(String token) throws Exception{
        Map<String,Object> maps = new HashMap<String,Object>() ;
        maps.put("access_token",token);
        maps.put("type",ConstantUtil.type);
        try{
            String result = com.xiaoleilu.hutool.http.HttpUtil.get(ConstantUtil.TICKET_URL_TEST,maps) ;
            JSONObject jsonObject = JSONHelper.toJSONObject(result) ;
            Integer errcode = (Integer) jsonObject.get("errcode") ;
            if(errcode==null || (errcode!=null &&errcode!=0)){
                return null ;
            }else{
                String ticket = (String) jsonObject.get("ticket") ;
                return ticket ;
            }
        }catch (Exception ex){
            throw new Exception("getJsApi Ticket is failed") ;
        }
    }
}
