package com.zhaodui.fxy;

import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class Base64Utils {




    public static boolean saveUrlAs(String fileUrl, String savePath,int imgsizeint)/* fileUrl网络资源地址 */
    {

        try {
            /* 将网络资源地址传给,即赋值给url */
            URL url = new URL(fileUrl);

            /* 此为联系获得网络资源的固定格式用法，以便后面的in变量获得url截取网络资源的输入流 */
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            DataInputStream in = new DataInputStream(connection.getInputStream());

            BufferedImage src = ImageIO.read(in);
            int width = src.getWidth();
            int height = src.getHeight();

             if(imgsizeint <= 0 ){
                 imgsizeint = 2;
             }

            // 边长缩小为二分之一
            BufferedImage tag = new BufferedImage(width / imgsizeint, height / imgsizeint, BufferedImage.TYPE_INT_RGB);
            // 绘制缩小后的图
            tag.getGraphics().drawImage(src, 0, 0, width / imgsizeint, height / imgsizeint, null);
//            FileOutputStream out1 = new FileOutputStream(savePath);
//            JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out1);
//            encoder.encode(tag);
//            out1.close();

            String formatName = savePath.substring(savePath.lastIndexOf(".") + 1);
            //FileOutputStream out = new FileOutputStream(dstName);
            //JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
            //encoder.encode(dstImage);
            ImageIO.write(tag, /*"GIF"*/ formatName /* format desired */ , new File(savePath) /* target */ );
            return true;/* 网络资源截取并存储本地成功返回true */

        } catch (Exception e) {
            System.out.println(e + fileUrl + savePath);
            return false;
        }
    }



    public static void download(String urlString, String filename) throws Exception {
        // 构造URL
        URL url = new URL(urlString);
        // 打开连接
        URLConnection con = url.openConnection();
        // 输入流
        InputStream is = con.getInputStream();
        // 1K的数据缓冲
        byte[] bs = new byte[1024];
        // 读取到的数据长度
        int len;
        // 输出的文件流
        OutputStream os = new FileOutputStream(filename);
        // 开始读取
        while ((len = is.read(bs)) != -1) {
            os.write(bs, 0, len);
        }
        // 完毕，关闭所有链接
        os.close();
        is.close();
    }

    //base64字符串转化成图片
    public static boolean GenerateImage(String imgStr,String imgFilePath )
    {   //对字节数组字符串进行Base64解码并生成图片
        if (imgStr == null) //图像数据为空
            return false;
        BASE64Decoder decoder = new BASE64Decoder();
        try
        {
            //Base64解码
            byte[] b = decoder.decodeBuffer(imgStr);
            for(int i=0;i<b.length;++i)
            {
                if(b[i]<0)
                {//调整异常数据
                    b[i]+=256;
                }
            }
            //生成jpeg图片
//            String imgFilePath = "d://222.jpg";//新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
}
