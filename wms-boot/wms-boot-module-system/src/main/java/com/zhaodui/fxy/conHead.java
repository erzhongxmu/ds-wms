package com.zhaodui.fxy;

import java.util.ArrayList;
import java.util.List;

public class conHead {

    private String conNo;
    private String orderNo;
    private String contempPath;
    private String contoPath;

    private String contoUrl;
    private String contopdfUrl;
    private String conBy1;
    private String conBy2;
    private String conBy3;
    private String conBy4;
    private String conBy5;
    private String conBy6;
    private String conBy7;
    private String conBy8;
    private String conBy9;
    private String conBy10;
    private String conBy11;
    private String conBy12;
    private String conBy13;
    private String conBy14;
    private String conBy15;
    private String conBy16;
    private String conBy17;
    private String conBy18;
    private String conBy19;
    private String conBy20;
    private String conBy21;
    private String conBy22;
    private String conBy23;
    private String conBy24;
    private String conBy25;
    private String conBy26;
    private String conBy27;
    private String conBy28;
    private String conBy29;
    private String conBy30;
    private String conBy31;
    private String conBy32;
    private String conBy33;
    private String conBy34;
    private String conBy35;
    private String conBy36;
    private String conBy37;
    private String conBy38;
    private String conBy39;
    private String conBy40;
    private String conBy41;
    private String conBy42;
    private String conBy43;
    private String conBy44;
    private String conBy45;
    private String conBy46;
    private String conBy47;
    private String conBy48;
    private String conBy49;
    private String conBy50;
    private String conBy51;
    private String conBy52;
    private String conBy53;
    private String conBy54;
    private String conBy55;
    private String conBy56;
    private String conBy57;
    private String conBy58;
    private String conBy59;
    private String conBy60;
    private String conBy61;
    private String conBy62;
    private String conBy63;
    private String conBy64;
    private String conBy65;
    private String conBy66;
    private String conBy67;
    private String conBy68;
    private String conBy69;
    private String conBy70;
    private String conBy71;
    private String conBy72;
    private String conBy73;
    private String conBy74;
    private String conBy75;
    private String conBy76;
    private String conBy77;
    private String conBy78;
    private String conBy79;
    private String conBy80;
    private String conBy81;
    private String conBy82;
    private String conBy83;
    private String conBy84;
    private String conBy85;
    private String conBy86;
    private String conBy87;
    private String conBy88;
    private String conBy89;
    private String conBy90;
    private String conBy91;
    private String conBy92;
    private String conBy93;
    private String conBy94;
    private String conBy95;
    private String conBy96;
    private String conBy97;
    private String conBy98;
    private String conBy99;

    public String getConNo() {
        return conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getConBy1() {
        return conBy1;
    }

    public void setConBy1(String conBy1) {
        this.conBy1 = conBy1;
    }

    public String getConBy2() {
        return conBy2;
    }

    public void setConBy2(String conBy2) {
        this.conBy2 = conBy2;
    }

    public String getConBy3() {
        return conBy3;
    }

    public void setConBy3(String conBy3) {
        this.conBy3 = conBy3;
    }

    public String getConBy4() {
        return conBy4;
    }

    public void setConBy4(String conBy4) {
        this.conBy4 = conBy4;
    }

    public String getConBy5() {
        return conBy5;
    }

    public void setConBy5(String conBy5) {
        this.conBy5 = conBy5;
    }

    public String getConBy6() {
        return conBy6;
    }

    public void setConBy6(String conBy6) {
        this.conBy6 = conBy6;
    }

    public String getConBy7() {
        return conBy7;
    }

    public void setConBy7(String conBy7) {
        this.conBy7 = conBy7;
    }

    public String getConBy8() {
        return conBy8;
    }

    public void setConBy8(String conBy8) {
        this.conBy8 = conBy8;
    }

    public String getConBy9() {
        return conBy9;
    }

    public void setConBy9(String conBy9) {
        this.conBy9 = conBy9;
    }

    public String getConBy10() {
        return conBy10;
    }

    public void setConBy10(String conBy10) {
        this.conBy10 = conBy10;
    }

    public String getConBy11() {
        return conBy11;
    }

    public void setConBy11(String conBy11) {
        this.conBy11 = conBy11;
    }

    public String getConBy12() {
        return conBy12;
    }

    public void setConBy12(String conBy12) {
        this.conBy12 = conBy12;
    }

    public String getConBy13() {
        return conBy13;
    }

    public void setConBy13(String conBy13) {
        this.conBy13 = conBy13;
    }

    public String getConBy14() {
        return conBy14;
    }

    public void setConBy14(String conBy14) {
        this.conBy14 = conBy14;
    }

    public String getConBy15() {
        return conBy15;
    }

    public void setConBy15(String conBy15) {
        this.conBy15 = conBy15;
    }

    public String getConBy16() {
        return conBy16;
    }

    public void setConBy16(String conBy16) {
        this.conBy16 = conBy16;
    }

    public String getConBy17() {
        return conBy17;
    }

    public void setConBy17(String conBy17) {
        this.conBy17 = conBy17;
    }

    public String getConBy18() {
        return conBy18;
    }

    public void setConBy18(String conBy18) {
        this.conBy18 = conBy18;
    }

    public String getConBy19() {
        return conBy19;
    }

    public void setConBy19(String conBy19) {
        this.conBy19 = conBy19;
    }

    public String getConBy20() {
        return conBy20;
    }

    public void setConBy20(String conBy20) {
        this.conBy20 = conBy20;
    }

    public String getConBy21() {
        return conBy21;
    }

    public void setConBy21(String conBy21) {
        this.conBy21 = conBy21;
    }

    public String getConBy22() {
        return conBy22;
    }

    public void setConBy22(String conBy22) {
        this.conBy22 = conBy22;
    }

    public String getConBy23() {
        return conBy23;
    }

    public void setConBy23(String conBy23) {
        this.conBy23 = conBy23;
    }

    public String getConBy24() {
        return conBy24;
    }

    public void setConBy24(String conBy24) {
        this.conBy24 = conBy24;
    }

    public String getConBy25() {
        return conBy25;
    }

    public void setConBy25(String conBy25) {
        this.conBy25 = conBy25;
    }

    public String getConBy26() {
        return conBy26;
    }

    public void setConBy26(String conBy26) {
        this.conBy26 = conBy26;
    }

    public String getConBy27() {
        return conBy27;
    }

    public void setConBy27(String conBy27) {
        this.conBy27 = conBy27;
    }

    public String getConBy28() {
        return conBy28;
    }

    public void setConBy28(String conBy28) {
        this.conBy28 = conBy28;
    }

    public String getConBy29() {
        return conBy29;
    }

    public void setConBy29(String conBy29) {
        this.conBy29 = conBy29;
    }

    public String getConBy30() {
        return conBy30;
    }

    public void setConBy30(String conBy30) {
        this.conBy30 = conBy30;
    }

    public String getConBy31() {
        return conBy31;
    }

    public void setConBy31(String conBy31) {
        this.conBy31 = conBy31;
    }

    public String getConBy32() {
        return conBy32;
    }

    public void setConBy32(String conBy32) {
        this.conBy32 = conBy32;
    }

    public String getConBy33() {
        return conBy33;
    }

    public void setConBy33(String conBy33) {
        this.conBy33 = conBy33;
    }

    public String getConBy34() {
        return conBy34;
    }

    public void setConBy34(String conBy34) {
        this.conBy34 = conBy34;
    }

    public String getConBy35() {
        return conBy35;
    }

    public void setConBy35(String conBy35) {
        this.conBy35 = conBy35;
    }

    public String getConBy36() {
        return conBy36;
    }

    public void setConBy36(String conBy36) {
        this.conBy36 = conBy36;
    }

    public String getConBy37() {
        return conBy37;
    }

    public void setConBy37(String conBy37) {
        this.conBy37 = conBy37;
    }

    public String getConBy38() {
        return conBy38;
    }

    public void setConBy38(String conBy38) {
        this.conBy38 = conBy38;
    }

    public String getConBy39() {
        return conBy39;
    }

    public void setConBy39(String conBy39) {
        this.conBy39 = conBy39;
    }

    public String getConBy40() {
        return conBy40;
    }

    public void setConBy40(String conBy40) {
        this.conBy40 = conBy40;
    }

    public String getConBy41() {
        return conBy41;
    }

    public void setConBy41(String conBy41) {
        this.conBy41 = conBy41;
    }

    public String getConBy42() {
        return conBy42;
    }

    public void setConBy42(String conBy42) {
        this.conBy42 = conBy42;
    }

    public String getConBy43() {
        return conBy43;
    }

    public void setConBy43(String conBy43) {
        this.conBy43 = conBy43;
    }

    public String getConBy44() {
        return conBy44;
    }

    public void setConBy44(String conBy44) {
        this.conBy44 = conBy44;
    }

    public String getConBy45() {
        return conBy45;
    }

    public void setConBy45(String conBy45) {
        this.conBy45 = conBy45;
    }

    public String getConBy46() {
        return conBy46;
    }

    public void setConBy46(String conBy46) {
        this.conBy46 = conBy46;
    }

    public String getConBy47() {
        return conBy47;
    }

    public void setConBy47(String conBy47) {
        this.conBy47 = conBy47;
    }

    public String getConBy48() {
        return conBy48;
    }

    public void setConBy48(String conBy48) {
        this.conBy48 = conBy48;
    }

    public String getConBy49() {
        return conBy49;
    }

    public void setConBy49(String conBy49) {
        this.conBy49 = conBy49;
    }

    public String getConBy50() {
        return conBy50;
    }

    public void setConBy50(String conBy50) {
        this.conBy50 = conBy50;
    }

    public String getConBy51() {
        return conBy51;
    }

    public void setConBy51(String conBy51) {
        this.conBy51 = conBy51;
    }

    public String getConBy52() {
        return conBy52;
    }

    public void setConBy52(String conBy52) {
        this.conBy52 = conBy52;
    }

    public String getConBy53() {
        return conBy53;
    }

    public void setConBy53(String conBy53) {
        this.conBy53 = conBy53;
    }

    public String getConBy54() {
        return conBy54;
    }

    public void setConBy54(String conBy54) {
        this.conBy54 = conBy54;
    }

    public String getConBy55() {
        return conBy55;
    }

    public void setConBy55(String conBy55) {
        this.conBy55 = conBy55;
    }

    public String getConBy56() {
        return conBy56;
    }

    public void setConBy56(String conBy56) {
        this.conBy56 = conBy56;
    }

    public String getConBy57() {
        return conBy57;
    }

    public void setConBy57(String conBy57) {
        this.conBy57 = conBy57;
    }

    public String getConBy58() {
        return conBy58;
    }

    public void setConBy58(String conBy58) {
        this.conBy58 = conBy58;
    }

    public String getConBy59() {
        return conBy59;
    }

    public void setConBy59(String conBy59) {
        this.conBy59 = conBy59;
    }

    public String getConBy60() {
        return conBy60;
    }

    public void setConBy60(String conBy60) {
        this.conBy60 = conBy60;
    }

    public String getConBy61() {
        return conBy61;
    }

    public void setConBy61(String conBy61) {
        this.conBy61 = conBy61;
    }

    public String getConBy62() {
        return conBy62;
    }

    public void setConBy62(String conBy62) {
        this.conBy62 = conBy62;
    }

    public String getConBy63() {
        return conBy63;
    }

    public void setConBy63(String conBy63) {
        this.conBy63 = conBy63;
    }

    public String getConBy64() {
        return conBy64;
    }

    public void setConBy64(String conBy64) {
        this.conBy64 = conBy64;
    }

    public String getConBy65() {
        return conBy65;
    }

    public void setConBy65(String conBy65) {
        this.conBy65 = conBy65;
    }

    public String getConBy66() {
        return conBy66;
    }

    public void setConBy66(String conBy66) {
        this.conBy66 = conBy66;
    }

    public String getConBy67() {
        return conBy67;
    }

    public void setConBy67(String conBy67) {
        this.conBy67 = conBy67;
    }

    public String getConBy68() {
        return conBy68;
    }

    public void setConBy68(String conBy68) {
        this.conBy68 = conBy68;
    }

    public String getConBy69() {
        return conBy69;
    }

    public void setConBy69(String conBy69) {
        this.conBy69 = conBy69;
    }

    public String getConBy70() {
        return conBy70;
    }

    public void setConBy70(String conBy70) {
        this.conBy70 = conBy70;
    }

    public String getConBy71() {
        return conBy71;
    }

    public void setConBy71(String conBy71) {
        this.conBy71 = conBy71;
    }

    public String getConBy72() {
        return conBy72;
    }

    public void setConBy72(String conBy72) {
        this.conBy72 = conBy72;
    }

    public String getConBy73() {
        return conBy73;
    }

    public void setConBy73(String conBy73) {
        this.conBy73 = conBy73;
    }

    public String getConBy74() {
        return conBy74;
    }

    public void setConBy74(String conBy74) {
        this.conBy74 = conBy74;
    }

    public String getConBy75() {
        return conBy75;
    }

    public void setConBy75(String conBy75) {
        this.conBy75 = conBy75;
    }

    public String getConBy76() {
        return conBy76;
    }

    public void setConBy76(String conBy76) {
        this.conBy76 = conBy76;
    }

    public String getConBy77() {
        return conBy77;
    }

    public void setConBy77(String conBy77) {
        this.conBy77 = conBy77;
    }

    public String getConBy78() {
        return conBy78;
    }

    public void setConBy78(String conBy78) {
        this.conBy78 = conBy78;
    }

    public String getConBy79() {
        return conBy79;
    }

    public void setConBy79(String conBy79) {
        this.conBy79 = conBy79;
    }

    public String getConBy80() {
        return conBy80;
    }

    public void setConBy80(String conBy80) {
        this.conBy80 = conBy80;
    }

    public String getConBy81() {
        return conBy81;
    }

    public void setConBy81(String conBy81) {
        this.conBy81 = conBy81;
    }

    public String getConBy82() {
        return conBy82;
    }

    public void setConBy82(String conBy82) {
        this.conBy82 = conBy82;
    }

    public String getConBy83() {
        return conBy83;
    }

    public void setConBy83(String conBy83) {
        this.conBy83 = conBy83;
    }

    public String getConBy84() {
        return conBy84;
    }

    public void setConBy84(String conBy84) {
        this.conBy84 = conBy84;
    }

    public String getConBy85() {
        return conBy85;
    }

    public void setConBy85(String conBy85) {
        this.conBy85 = conBy85;
    }

    public String getConBy86() {
        return conBy86;
    }

    public void setConBy86(String conBy86) {
        this.conBy86 = conBy86;
    }

    public String getConBy87() {
        return conBy87;
    }

    public void setConBy87(String conBy87) {
        this.conBy87 = conBy87;
    }

    public String getConBy88() {
        return conBy88;
    }

    public void setConBy88(String conBy88) {
        this.conBy88 = conBy88;
    }

    public String getConBy89() {
        return conBy89;
    }

    public void setConBy89(String conBy89) {
        this.conBy89 = conBy89;
    }

    public String getConBy90() {
        return conBy90;
    }

    public void setConBy90(String conBy90) {
        this.conBy90 = conBy90;
    }

    public String getConBy91() {
        return conBy91;
    }

    public void setConBy91(String conBy91) {
        this.conBy91 = conBy91;
    }

    public String getConBy92() {
        return conBy92;
    }

    public void setConBy92(String conBy92) {
        this.conBy92 = conBy92;
    }

    public String getConBy93() {
        return conBy93;
    }

    public void setConBy93(String conBy93) {
        this.conBy93 = conBy93;
    }

    public String getConBy94() {
        return conBy94;
    }

    public void setConBy94(String conBy94) {
        this.conBy94 = conBy94;
    }

    public String getConBy95() {
        return conBy95;
    }

    public void setConBy95(String conBy95) {
        this.conBy95 = conBy95;
    }

    public String getConBy96() {
        return conBy96;
    }

    public void setConBy96(String conBy96) {
        this.conBy96 = conBy96;
    }

    public String getConBy97() {
        return conBy97;
    }

    public void setConBy97(String conBy97) {
        this.conBy97 = conBy97;
    }

    public String getConBy98() {
        return conBy98;
    }

    public void setConBy98(String conBy98) {
        this.conBy98 = conBy98;
    }

    public String getConBy99() {
        return conBy99;
    }

    public void setConBy99(String conBy99) {
        this.conBy99 = conBy99;
    }

    private List<conmap> conmapList = new ArrayList<conmap>();

    public List<conmap> getConmapList() {
        return conmapList;
    }

    public void setConmapList(List<conmap> conmapList) {
        this.conmapList = conmapList;
    }

    public String getContempPath() {
        return contempPath;
    }

    public void setContempPath(String contempPath) {
        this.contempPath = contempPath;
    }

    public String getContoPath() {
        return contoPath;
    }

    public void setContoPath(String contoPath) {
        this.contoPath = contoPath;
    }

    public String getContoUrl() {
        return contoUrl;
    }

    public void setContoUrl(String contoUrl) {
        this.contoUrl = contoUrl;
    }

    public String getContopdfUrl() {
        return contopdfUrl;
    }

    public void setContopdfUrl(String contopdfUrl) {
        this.contopdfUrl = contopdfUrl;
    }
}
