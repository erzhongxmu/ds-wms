package com.zhaodui.fxy;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParseException;

import java.io.IOException;

;

/**
 * JSON和JAVA的POJO的相互转换
 * @author  张代浩
 * JSONHelper.java
 */
public final class JSONHelper {

	/**
	 * json转换为java对象
	 *
	 * <pre>
	 * return JackJson.fromJsonToObject(this.answersJson, JackJson.class);
	 * </pre>
	 *
	 * @param <T>
	 *            要转换的对象
	 * @param json
	 *            字符串
	 * @param valueType
	 *            对象的class
	 * @return 返回对象
	 */
	public static <T> T fromJsonToObject(String json, Class<T> valueType) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, valueType);
		} catch (JsonParseException e) {
 		} catch (JsonMappingException e) {
 		} catch (IOException e) {
 		}
		return null;
	}
	/***
	 * 将对象转换为JSON对象
	 *
	 * @param object
	 * @return
	 */
	public static net.sf.json.JSONObject toJSONObject(Object object) {
		return net.sf.json.JSONObject.fromObject(object);
	}
}
