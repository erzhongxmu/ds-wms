package com.zhaodui.fxy;

public class BaseBusinessException extends RuntimeException {


    private String code;

    private String message;

    public BaseBusinessException(String code,String message) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
