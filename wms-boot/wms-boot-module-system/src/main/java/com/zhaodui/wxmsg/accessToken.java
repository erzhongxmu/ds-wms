package com.zhaodui.wxmsg;


import com.google.gson.annotations.SerializedName;

public class accessToken {

    /**
     * access_token : 18_t4KNEXqxq_G-YoXsjSVUBfAKOzR62UAhbqE6SWtfCkUmI2iiecH6dVtVg3zW5ftJM5yFoIAYKAOBUVXXJ2deBiUG0vzGs5Kt26zoehAPZIMdYJC_y0pp4p3oZho20VPGGaEz_W4ZQi874f4iIURhAAALXU
     * expires_in : 7200
     */

    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("expires_in")
    private String expiresIn;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }
}
