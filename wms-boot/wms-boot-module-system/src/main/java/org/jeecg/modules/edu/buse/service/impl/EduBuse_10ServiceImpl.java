package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_10;
import org.jeecg.modules.edu.buse.mapper.EduBuse_10Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_10Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 课程
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_10ServiceImpl extends ServiceImpl<EduBuse_10Mapper, EduBuse_10> implements IEduBuse_10Service {

}
