package org.jeecg.modules.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.base.entity.WxConfigEntity;
import org.jeecg.modules.base.service.IWxConfigEntityService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 微信配置
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Api(tags="微信配置")
@RestController
@RequestMapping("/base/wxConfigEntity")
@Slf4j
public class WxConfigEntityController extends JeecgController<WxConfigEntity, IWxConfigEntityService> {
	@Autowired
	private IWxConfigEntityService wxConfigEntityService;

	/**
	 * 分页列表查询
	 *
	 * @param wxConfigEntity
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "微信配置-分页列表查询")
	@ApiOperation(value="微信配置-分页列表查询", notes="微信配置-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WxConfigEntity wxConfigEntity,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WxConfigEntity> queryWrapper = QueryGenerator.initQueryWrapper(wxConfigEntity, req.getParameterMap());
		Page<WxConfigEntity> page = new Page<WxConfigEntity>(pageNo, pageSize);
		IPage<WxConfigEntity> pageList = wxConfigEntityService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param wxConfigEntity
	 * @return
	 */
	@AutoLog(value = "微信配置-添加")
	@ApiOperation(value="微信配置-添加", notes="微信配置-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WxConfigEntity wxConfigEntity) {
		wxConfigEntityService.save(wxConfigEntity);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param wxConfigEntity
	 * @return
	 */
	@AutoLog(value = "微信配置-编辑")
	@ApiOperation(value="微信配置-编辑", notes="微信配置-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WxConfigEntity wxConfigEntity) {
		wxConfigEntityService.updateById(wxConfigEntity);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "微信配置-通过id删除")
	@ApiOperation(value="微信配置-通过id删除", notes="微信配置-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		wxConfigEntityService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "微信配置-批量删除")
	@ApiOperation(value="微信配置-批量删除", notes="微信配置-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.wxConfigEntityService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "微信配置-通过id查询")
	@ApiOperation(value="微信配置-通过id查询", notes="微信配置-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WxConfigEntity wxConfigEntity = wxConfigEntityService.getById(id);
		if(wxConfigEntity==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(wxConfigEntity);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param wxConfigEntity
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WxConfigEntity wxConfigEntity) {
        return super.exportXls(request, wxConfigEntity, WxConfigEntity.class, "微信配置");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WxConfigEntity.class);
    }

}
