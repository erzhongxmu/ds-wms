package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_09;
import org.jeecg.modules.edu.buse.mapper.EduBuse_09Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_09Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 师资
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_09ServiceImpl extends ServiceImpl<EduBuse_09Mapper, EduBuse_09> implements IEduBuse_09Service {

}
