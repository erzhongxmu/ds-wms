package org.jeecg.modules.edu.base.service.impl;

import org.jeecg.modules.edu.base.entity.EduBase_02;
import org.jeecg.modules.edu.base.mapper.EduBase_02Mapper;
import org.jeecg.modules.edu.base.service.IEduBase_02Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 金刚区
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBase_02ServiceImpl extends ServiceImpl<EduBase_02Mapper, EduBase_02> implements IEduBase_02Service {

}
