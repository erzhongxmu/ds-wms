package org.jeecg.modules.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.base.entity.WxConfigEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 微信配置
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface WxConfigEntityMapper extends BaseMapper<WxConfigEntity> {
    public WxConfigEntity getUserByappcode(@Param("appcode") String appcode);

}
