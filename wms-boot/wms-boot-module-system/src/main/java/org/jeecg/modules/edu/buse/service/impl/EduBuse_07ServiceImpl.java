package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_07;
import org.jeecg.modules.edu.buse.mapper.EduBuse_07Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_07Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 会员
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_07ServiceImpl extends ServiceImpl<EduBuse_07Mapper, EduBuse_07> implements IEduBuse_07Service {

}
