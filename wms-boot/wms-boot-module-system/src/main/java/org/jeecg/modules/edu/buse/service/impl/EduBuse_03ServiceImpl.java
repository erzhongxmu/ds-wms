package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_03;
import org.jeecg.modules.edu.buse.mapper.EduBuse_03Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_03Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 发布活动
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_03ServiceImpl extends ServiceImpl<EduBuse_03Mapper, EduBuse_03> implements IEduBuse_03Service {

}
