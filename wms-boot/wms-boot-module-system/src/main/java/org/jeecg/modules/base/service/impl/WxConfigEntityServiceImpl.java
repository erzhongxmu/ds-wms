package org.jeecg.modules.base.service.impl;

import org.jeecg.modules.base.entity.WxConfigEntity;
import org.jeecg.modules.base.mapper.WxConfigEntityMapper;
import org.jeecg.modules.base.service.IWxConfigEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 微信配置
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class WxConfigEntityServiceImpl extends ServiceImpl<WxConfigEntityMapper, WxConfigEntity> implements IWxConfigEntityService {
    @Autowired
    private  WxConfigEntityMapper wxConfigEntityMapper;
    @Override
    public WxConfigEntity getUserByappcode(String appcode) {
        return wxConfigEntityMapper.getUserByappcode(appcode);
    }

}
