package org.jeecg.modules.edu.buse.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.buse.entity.EduBuse_05;
import org.jeecg.modules.edu.buse.service.IEduBuse_05Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 分享
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Api(tags="分享")
@RestController
@RequestMapping("/buse/eduBuse_05")
@Slf4j
public class EduBuse_05Controller extends JeecgController<EduBuse_05, IEduBuse_05Service> {
	@Autowired
	private IEduBuse_05Service eduBuse_05Service;
	
	/**
	 * 分页列表查询
	 *
	 * @param eduBuse_05
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "分享-分页列表查询")
	@ApiOperation(value="分享-分页列表查询", notes="分享-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(EduBuse_05 eduBuse_05,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<EduBuse_05> queryWrapper = QueryGenerator.initQueryWrapper(eduBuse_05, req.getParameterMap());
		Page<EduBuse_05> page = new Page<EduBuse_05>(pageNo, pageSize);
		IPage<EduBuse_05> pageList = eduBuse_05Service.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param eduBuse_05
	 * @return
	 */
	@AutoLog(value = "分享-添加")
	@ApiOperation(value="分享-添加", notes="分享-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody EduBuse_05 eduBuse_05) {
		eduBuse_05Service.save(eduBuse_05);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param eduBuse_05
	 * @return
	 */
	@AutoLog(value = "分享-编辑")
	@ApiOperation(value="分享-编辑", notes="分享-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody EduBuse_05 eduBuse_05) {
		eduBuse_05Service.updateById(eduBuse_05);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "分享-通过id删除")
	@ApiOperation(value="分享-通过id删除", notes="分享-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		eduBuse_05Service.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "分享-批量删除")
	@ApiOperation(value="分享-批量删除", notes="分享-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.eduBuse_05Service.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "分享-通过id查询")
	@ApiOperation(value="分享-通过id查询", notes="分享-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		EduBuse_05 eduBuse_05 = eduBuse_05Service.getById(id);
		if(eduBuse_05==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(eduBuse_05);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param eduBuse_05
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, EduBuse_05 eduBuse_05) {
        return super.exportXls(request, eduBuse_05, EduBuse_05.class, "分享");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, EduBuse_05.class);
    }

}
