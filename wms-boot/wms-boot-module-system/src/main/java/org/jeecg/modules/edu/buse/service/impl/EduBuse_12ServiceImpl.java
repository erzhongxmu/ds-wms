package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_12;
import org.jeecg.modules.edu.buse.mapper.EduBuse_12Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_12Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 资讯
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_12ServiceImpl extends ServiceImpl<EduBuse_12Mapper, EduBuse_12> implements IEduBuse_12Service {

}
