package org.jeecg.modules.edu.base.service;

import org.jeecg.modules.edu.base.entity.EduBase_01;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 城市
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBase_01Service extends IService<EduBase_01> {

}
