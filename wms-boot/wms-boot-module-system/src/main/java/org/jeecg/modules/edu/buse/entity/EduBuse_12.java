package org.jeecg.modules.edu.buse.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 资讯
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Data
@TableName("edu_buse_12")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="edu_buse_12对象", description="资讯")
public class EduBuse_12 implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
    @ApiModelProperty(value = "租户ID")
    private String query01;
	/**是否有效*/
	@Excel(name = "是否有效", width = 15)
    @ApiModelProperty(value = "是否有效")
    private String query02;
	/**所属机构*/
	@Excel(name = "所属机构", width = 15)
    @ApiModelProperty(value = "所属机构")
    private String query03;
	/**机构分类*/
	@Excel(name = "机构分类", width = 15)
    @ApiModelProperty(value = "机构分类")
    private String query04;
	/**机构详情*/
	@Excel(name = "机构详情", width = 15)
    @ApiModelProperty(value = "机构详情")
    private String query05;
	/**机构图片*/
	@Excel(name = "机构图片", width = 15)
    @ApiModelProperty(value = "机构图片")
    private String query06;
	/**机构地址*/
	@Excel(name = "机构地址", width = 15)
    @ApiModelProperty(value = "机构地址")
    private String query07;
	/**机构电话*/
	@Excel(name = "机构电话", width = 15)
    @ApiModelProperty(value = "机构电话")
    private String query08;
	/**机构联系人*/
	@Excel(name = "机构联系人", width = 15)
    @ApiModelProperty(value = "机构联系人")
    private String query09;
	/**详情*/
	@Excel(name = "详情", width = 15)
    @ApiModelProperty(value = "详情")
    private String query10;
	/**视频*/
	@Excel(name = "视频", width = 15)
    @ApiModelProperty(value = "视频")
    private String query11;
	/**分类*/
	@Excel(name = "分类", width = 15)
    @ApiModelProperty(value = "分类")
    private String query12;
	/**简介*/
	@Excel(name = "简介", width = 15)
    @ApiModelProperty(value = "简介")
    private String query13;
	/**banner*/
	@Excel(name = "banner", width = 15)
    @ApiModelProperty(value = "banner")
    private String query14;
	/**推荐*/
	@Excel(name = "推荐", width = 15)
    @ApiModelProperty(value = "推荐")
    private String query15;
	/**发现*/
	@Excel(name = "发现", width = 15)
    @ApiModelProperty(value = "发现")
    private String query16;
	/**赛事*/
	@Excel(name = "赛事", width = 15)
    @ApiModelProperty(value = "赛事")
    private String query17;
	/**猜你喜欢*/
	@Excel(name = "猜你喜欢", width = 15)
    @ApiModelProperty(value = "猜你喜欢")
    private String query18;
	/**在看*/
	@Excel(name = "在看", width = 15)
    @ApiModelProperty(value = "在看")
    private String query19;
	/**已读*/
	@Excel(name = "已读", width = 15)
    @ApiModelProperty(value = "已读")
    private String query20;
	/**转发*/
	@Excel(name = "转发", width = 15)
    @ApiModelProperty(value = "转发")
    private String query21;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query22;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query23;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query24;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query25;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query26;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query27;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query28;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query29;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query30;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query31;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query32;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query33;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query34;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query35;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query36;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query37;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query38;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query39;
}
