package org.jeecg.modules.edu.buse.service;

import org.jeecg.modules.edu.buse.entity.EduBuse_04;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 发现
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBuse_04Service extends IService<EduBuse_04> {

}
