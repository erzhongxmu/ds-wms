package org.jeecg.modules.edu.base.service.impl;

import org.jeecg.modules.edu.base.entity.EduBase_01;
import org.jeecg.modules.edu.base.mapper.EduBase_01Mapper;
import org.jeecg.modules.edu.base.service.IEduBase_01Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 城市
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBase_01ServiceImpl extends ServiceImpl<EduBase_01Mapper, EduBase_01> implements IEduBase_01Service {

}
