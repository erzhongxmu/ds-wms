package org.jeecg.modules.edu.buse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.buse.entity.EduBuse_14;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 操作日志
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface EduBuse_14Mapper extends BaseMapper<EduBuse_14> {

}
