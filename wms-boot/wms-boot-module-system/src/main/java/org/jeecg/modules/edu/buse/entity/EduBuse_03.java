package org.jeecg.modules.edu.buse.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 发布活动
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Data
@TableName("edu_buse_03")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="edu_buse_03对象", description="发布活动")
public class EduBuse_03 implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**租户ID*/
	@Excel(name = "租户ID", width = 15)
    @ApiModelProperty(value = "租户ID")
    private String query01;
	/**是否有效*/
	@Excel(name = "是否有效", width = 15)
    @ApiModelProperty(value = "是否有效")
    private String query02;
	/**微信ID*/
	@Excel(name = "微信ID", width = 15)
    @ApiModelProperty(value = "微信ID")
    private String query03;
	/**电话*/
	@Excel(name = "电话", width = 15)
    @ApiModelProperty(value = "电话")
    private String query04;
	/**姓名*/
	@Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String query05;
	/**海报*/
	@Excel(name = "海报", width = 15)
    @ApiModelProperty(value = "海报")
    private String query06;
	/**开始时间*/
	@Excel(name = "开始时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private Date query07;
	/**结束时间*/
	@Excel(name = "结束时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间")
    private Date query08;
	/**费用*/
	@Excel(name = "费用", width = 15)
    @ApiModelProperty(value = "费用")
    private String query09;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private String query10;
	/**地点*/
	@Excel(name = "地点", width = 15)
    @ApiModelProperty(value = "地点")
    private String query11;
	/**详情*/
	@Excel(name = "详情", width = 15)
    @ApiModelProperty(value = "详情")
    private String query12;
	/**限制人数*/
	@Excel(name = "限制人数", width = 15)
    @ApiModelProperty(value = "限制人数")
    private String query13;
	/**banner*/
	@Excel(name = "banner", width = 15)
    @ApiModelProperty(value = "banner")
    private String query14;
	/**推荐*/
	@Excel(name = "推荐", width = 15)
    @ApiModelProperty(value = "推荐")
    private String query15;
	/**发现*/
	@Excel(name = "发现", width = 15)
    @ApiModelProperty(value = "发现")
    private String query16;
	/**赛事*/
	@Excel(name = "赛事", width = 15)
    @ApiModelProperty(value = "赛事")
    private String query17;
	/**猜你喜欢*/
	@Excel(name = "猜你喜欢", width = 15)
    @ApiModelProperty(value = "猜你喜欢")
    private String query18;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query19;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query20;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query21;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query22;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query23;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query24;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query25;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query26;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query27;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query28;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query29;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query30;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query31;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query32;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query33;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query34;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query35;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query36;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query37;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query38;
	/**备用12*/
	@Excel(name = "备用12", width = 15)
    @ApiModelProperty(value = "备用12")
    private String query39;
}
