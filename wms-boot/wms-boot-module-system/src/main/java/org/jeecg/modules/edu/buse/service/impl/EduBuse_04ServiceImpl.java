package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_04;
import org.jeecg.modules.edu.buse.mapper.EduBuse_04Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_04Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 发现
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_04ServiceImpl extends ServiceImpl<EduBuse_04Mapper, EduBuse_04> implements IEduBuse_04Service {

}
