package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_13;
import org.jeecg.modules.edu.buse.mapper.EduBuse_13Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_13Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 订单
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_13ServiceImpl extends ServiceImpl<EduBuse_13Mapper, EduBuse_13> implements IEduBuse_13Service {

}
