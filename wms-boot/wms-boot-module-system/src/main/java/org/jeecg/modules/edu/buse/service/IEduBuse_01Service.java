package org.jeecg.modules.edu.buse.service;

import org.jeecg.modules.edu.buse.entity.EduBuse_01;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 充值中心
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBuse_01Service extends IService<EduBuse_01> {

}
