package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_01;
import org.jeecg.modules.edu.buse.mapper.EduBuse_01Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_01Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 充值中心
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_01ServiceImpl extends ServiceImpl<EduBuse_01Mapper, EduBuse_01> implements IEduBuse_01Service {

}
