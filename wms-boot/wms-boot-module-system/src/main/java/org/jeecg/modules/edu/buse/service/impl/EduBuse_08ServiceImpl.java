package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_08;
import org.jeecg.modules.edu.buse.mapper.EduBuse_08Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_08Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 机构
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_08ServiceImpl extends ServiceImpl<EduBuse_08Mapper, EduBuse_08> implements IEduBuse_08Service {

}
