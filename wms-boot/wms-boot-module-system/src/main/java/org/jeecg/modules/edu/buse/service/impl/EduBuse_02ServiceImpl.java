package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_02;
import org.jeecg.modules.edu.buse.mapper.EduBuse_02Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_02Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 兑换记录
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_02ServiceImpl extends ServiceImpl<EduBuse_02Mapper, EduBuse_02> implements IEduBuse_02Service {

}
