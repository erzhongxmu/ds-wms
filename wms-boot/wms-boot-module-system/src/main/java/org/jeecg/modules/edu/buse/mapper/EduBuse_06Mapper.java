package org.jeecg.modules.edu.buse.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.buse.entity.EduBuse_06;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 粉丝管理
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface EduBuse_06Mapper extends BaseMapper<EduBuse_06> {

}
