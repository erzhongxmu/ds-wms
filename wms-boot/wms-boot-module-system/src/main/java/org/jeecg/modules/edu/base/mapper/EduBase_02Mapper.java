package org.jeecg.modules.edu.base.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.edu.base.entity.EduBase_02;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 金刚区
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface EduBase_02Mapper extends BaseMapper<EduBase_02> {

}
