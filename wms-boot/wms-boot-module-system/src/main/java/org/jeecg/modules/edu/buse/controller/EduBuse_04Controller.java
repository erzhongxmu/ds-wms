package org.jeecg.modules.edu.buse.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.buse.entity.EduBuse_04;
import org.jeecg.modules.edu.buse.service.IEduBuse_04Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 发现
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Api(tags="发现")
@RestController
@RequestMapping("/buse/eduBuse_04")
@Slf4j
public class EduBuse_04Controller extends JeecgController<EduBuse_04, IEduBuse_04Service> {
	@Autowired
	private IEduBuse_04Service eduBuse_04Service;
	
	/**
	 * 分页列表查询
	 *
	 * @param eduBuse_04
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "发现-分页列表查询")
	@ApiOperation(value="发现-分页列表查询", notes="发现-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(EduBuse_04 eduBuse_04,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<EduBuse_04> queryWrapper = QueryGenerator.initQueryWrapper(eduBuse_04, req.getParameterMap());
		Page<EduBuse_04> page = new Page<EduBuse_04>(pageNo, pageSize);
		IPage<EduBuse_04> pageList = eduBuse_04Service.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param eduBuse_04
	 * @return
	 */
	@AutoLog(value = "发现-添加")
	@ApiOperation(value="发现-添加", notes="发现-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody EduBuse_04 eduBuse_04) {
		eduBuse_04Service.save(eduBuse_04);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param eduBuse_04
	 * @return
	 */
	@AutoLog(value = "发现-编辑")
	@ApiOperation(value="发现-编辑", notes="发现-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody EduBuse_04 eduBuse_04) {
		eduBuse_04Service.updateById(eduBuse_04);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "发现-通过id删除")
	@ApiOperation(value="发现-通过id删除", notes="发现-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		eduBuse_04Service.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "发现-批量删除")
	@ApiOperation(value="发现-批量删除", notes="发现-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.eduBuse_04Service.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "发现-通过id查询")
	@ApiOperation(value="发现-通过id查询", notes="发现-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		EduBuse_04 eduBuse_04 = eduBuse_04Service.getById(id);
		if(eduBuse_04==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(eduBuse_04);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param eduBuse_04
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, EduBuse_04 eduBuse_04) {
        return super.exportXls(request, eduBuse_04, EduBuse_04.class, "发现");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, EduBuse_04.class);
    }

}
