package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_05;
import org.jeecg.modules.edu.buse.mapper.EduBuse_05Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_05Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 分享
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_05ServiceImpl extends ServiceImpl<EduBuse_05Mapper, EduBuse_05> implements IEduBuse_05Service {

}
