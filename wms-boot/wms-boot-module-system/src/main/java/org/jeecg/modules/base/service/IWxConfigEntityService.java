package org.jeecg.modules.base.service;

import org.jeecg.modules.base.entity.WxConfigEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 微信配置
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IWxConfigEntityService extends IService<WxConfigEntity> {
    public WxConfigEntity getUserByappcode(String appcode);

}
