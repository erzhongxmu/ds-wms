package org.jeecg.modules.edu.buse.service;

import org.jeecg.modules.edu.buse.entity.EduBuse_06;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 粉丝管理
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBuse_06Service extends IService<EduBuse_06> {

}
