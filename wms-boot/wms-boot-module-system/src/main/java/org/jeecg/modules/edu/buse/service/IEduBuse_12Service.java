package org.jeecg.modules.edu.buse.service;

import org.jeecg.modules.edu.buse.entity.EduBuse_12;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 资讯
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBuse_12Service extends IService<EduBuse_12> {

}
