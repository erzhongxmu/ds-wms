package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_11;
import org.jeecg.modules.edu.buse.mapper.EduBuse_11Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_11Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 积分
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_11ServiceImpl extends ServiceImpl<EduBuse_11Mapper, EduBuse_11> implements IEduBuse_11Service {

}
