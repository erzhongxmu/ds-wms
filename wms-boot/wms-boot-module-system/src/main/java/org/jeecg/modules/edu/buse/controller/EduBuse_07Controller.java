package org.jeecg.modules.edu.buse.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.buse.entity.EduBuse_07;
import org.jeecg.modules.edu.buse.service.IEduBuse_07Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 会员
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Api(tags="会员")
@RestController
@RequestMapping("/buse/eduBuse_07")
@Slf4j
public class EduBuse_07Controller extends JeecgController<EduBuse_07, IEduBuse_07Service> {
	@Autowired
	private IEduBuse_07Service eduBuse_07Service;
	
	/**
	 * 分页列表查询
	 *
	 * @param eduBuse_07
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "会员-分页列表查询")
	@ApiOperation(value="会员-分页列表查询", notes="会员-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(EduBuse_07 eduBuse_07,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<EduBuse_07> queryWrapper = QueryGenerator.initQueryWrapper(eduBuse_07, req.getParameterMap());
		Page<EduBuse_07> page = new Page<EduBuse_07>(pageNo, pageSize);
		IPage<EduBuse_07> pageList = eduBuse_07Service.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param eduBuse_07
	 * @return
	 */
	@AutoLog(value = "会员-添加")
	@ApiOperation(value="会员-添加", notes="会员-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody EduBuse_07 eduBuse_07) {
		eduBuse_07Service.save(eduBuse_07);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param eduBuse_07
	 * @return
	 */
	@AutoLog(value = "会员-编辑")
	@ApiOperation(value="会员-编辑", notes="会员-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody EduBuse_07 eduBuse_07) {
		eduBuse_07Service.updateById(eduBuse_07);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员-通过id删除")
	@ApiOperation(value="会员-通过id删除", notes="会员-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		eduBuse_07Service.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "会员-批量删除")
	@ApiOperation(value="会员-批量删除", notes="会员-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.eduBuse_07Service.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "会员-通过id查询")
	@ApiOperation(value="会员-通过id查询", notes="会员-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		EduBuse_07 eduBuse_07 = eduBuse_07Service.getById(id);
		if(eduBuse_07==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(eduBuse_07);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param eduBuse_07
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, EduBuse_07 eduBuse_07) {
        return super.exportXls(request, eduBuse_07, EduBuse_07.class, "会员");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, EduBuse_07.class);
    }

}
