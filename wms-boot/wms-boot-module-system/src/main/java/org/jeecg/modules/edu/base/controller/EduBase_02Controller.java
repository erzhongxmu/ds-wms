package org.jeecg.modules.edu.base.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.edu.base.entity.EduBase_02;
import org.jeecg.modules.edu.base.service.IEduBase_02Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 金刚区
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Api(tags="金刚区")
@RestController
@RequestMapping("/base/eduBase_02")
@Slf4j
public class EduBase_02Controller extends JeecgController<EduBase_02, IEduBase_02Service> {
	@Autowired
	private IEduBase_02Service eduBase_02Service;
	
	/**
	 * 分页列表查询
	 *
	 * @param eduBase_02
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "金刚区-分页列表查询")
	@ApiOperation(value="金刚区-分页列表查询", notes="金刚区-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(EduBase_02 eduBase_02,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<EduBase_02> queryWrapper = QueryGenerator.initQueryWrapper(eduBase_02, req.getParameterMap());
		Page<EduBase_02> page = new Page<EduBase_02>(pageNo, pageSize);
		IPage<EduBase_02> pageList = eduBase_02Service.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param eduBase_02
	 * @return
	 */
	@AutoLog(value = "金刚区-添加")
	@ApiOperation(value="金刚区-添加", notes="金刚区-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody EduBase_02 eduBase_02) {
		eduBase_02Service.save(eduBase_02);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param eduBase_02
	 * @return
	 */
	@AutoLog(value = "金刚区-编辑")
	@ApiOperation(value="金刚区-编辑", notes="金刚区-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody EduBase_02 eduBase_02) {
		eduBase_02Service.updateById(eduBase_02);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "金刚区-通过id删除")
	@ApiOperation(value="金刚区-通过id删除", notes="金刚区-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		eduBase_02Service.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "金刚区-批量删除")
	@ApiOperation(value="金刚区-批量删除", notes="金刚区-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.eduBase_02Service.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "金刚区-通过id查询")
	@ApiOperation(value="金刚区-通过id查询", notes="金刚区-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		EduBase_02 eduBase_02 = eduBase_02Service.getById(id);
		if(eduBase_02==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(eduBase_02);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param eduBase_02
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, EduBase_02 eduBase_02) {
        return super.exportXls(request, eduBase_02, EduBase_02.class, "金刚区");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, EduBase_02.class);
    }

}
