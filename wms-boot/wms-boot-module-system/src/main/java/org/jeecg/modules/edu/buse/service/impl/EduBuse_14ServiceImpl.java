package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_14;
import org.jeecg.modules.edu.buse.mapper.EduBuse_14Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_14Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 操作日志
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_14ServiceImpl extends ServiceImpl<EduBuse_14Mapper, EduBuse_14> implements IEduBuse_14Service {

}
