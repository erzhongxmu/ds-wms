package org.jeecg.modules.edu.buse.service;

import org.jeecg.modules.edu.buse.entity.EduBuse_14;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 操作日志
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBuse_14Service extends IService<EduBuse_14> {

}
