package org.jeecg.modules.edu.buse.service.impl;

import org.jeecg.modules.edu.buse.entity.EduBuse_06;
import org.jeecg.modules.edu.buse.mapper.EduBuse_06Mapper;
import org.jeecg.modules.edu.buse.service.IEduBuse_06Service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 粉丝管理
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
@Service
public class EduBuse_06ServiceImpl extends ServiceImpl<EduBuse_06Mapper, EduBuse_06> implements IEduBuse_06Service {

}
