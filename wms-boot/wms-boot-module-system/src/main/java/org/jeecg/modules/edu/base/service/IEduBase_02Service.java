package org.jeecg.modules.edu.base.service;

import org.jeecg.modules.edu.base.entity.EduBase_02;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 金刚区
 * @Author: jeecg-boot
 * @Date:   2020-05-10
 * @Version: V1.0
 */
public interface IEduBase_02Service extends IService<EduBase_02> {

}
